variable "role_name" {
  description = "Name for the Lambda role."
  default     = "las-role"
}

variable "function_name" {
  description = "Name for the Lambda function."
  default     = "las-sendmail"
}

variable "billing_tag" {
  description = "Name for a tag to keep track of resource for billing."
  default     = "las"
}

variable "api_gateway_name" {
  description = "Name for your API gateway."
  default     = "las"
}

variable "api_gateway_description" {
  description = "Description of your API gateway."
  default     = "API gateway for las"
}
