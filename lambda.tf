resource "aws_lambda_function" "sendmail" {
  filename      = "exports.js.zip"
  function_name = "${var.function_name}"
  role          = "${aws_iam_role.iam_for_lambda.arn}"
  handler       = "exports.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = "${filebase64sha256("exports.js.zip")}"

  runtime = "nodejs8.10"

  environment {
    variables = {
      billing = "${var.billing_tag}"
    }
  }
}
